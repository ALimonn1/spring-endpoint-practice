package com.example.controllers;

import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class MainController {
    @GetMapping("/camelize")
    public @ResponseBody
    String camelize(@RequestParam String original,
                    @RequestParam(required = false, defaultValue = "false") boolean initialCap) {
        String convertedStr = convertToPascalCase(original);
        if (initialCap) {
            return convertedStr;
        }
        return Character.toLowerCase(convertedStr.charAt(0)) + convertedStr.substring(1);

    }

    @GetMapping("/redact")
    public @ResponseBody
    String redact(@RequestParam String original,
                  @RequestParam(name = "badWord") List<String> badWords) {
        Set<String> checkForBadWord = new HashSet<>(badWords);
        String badWordCover = "*";
        String[] words = original.split(" ");
        List<String> wordsToPutBack = new ArrayList<>();
        for (String word : words) {
            if (checkForBadWord.contains(word)) {
                wordsToPutBack.add(new String(new char[word.length()]).replace("\0", badWordCover));
            } else {
                wordsToPutBack.add(word);
            }
        }
        return String.join(" ", wordsToPutBack);
    }

    @PostMapping("/encode")
    public @ResponseBody
    String encode(@RequestParam String message, @RequestParam String key) {
        Map<Character, Character> alphabetMapping = new HashMap<>();
        char alphabetCursor = 'a';

        for (char c : key.toCharArray()) {
            alphabetMapping.put(alphabetCursor++, c);
        }
        alphabetMapping.put(' ', ' ');
        StringBuilder newMessageBuilder = new StringBuilder();
        for (char c : message.toCharArray()) {
            if (!alphabetMapping.containsKey(c)) {
                newMessageBuilder.append(c);
            } else {
                newMessageBuilder.append(alphabetMapping.get(c));
            }
        }
        return newMessageBuilder.toString();
    }

    @PostMapping("/s/{find}/{replace}")
    public @ResponseBody
    String sed(@RequestBody String document, @PathVariable String find, @PathVariable String replace){
        return document.replaceAll(find, replace);
    }

    private String convertToPascalCase(String original) {
        String[] words = original.split("_");
        StringBuilder stringBuilder = new StringBuilder();
        for (String word : words) {
            stringBuilder.append(Character.toUpperCase(word.charAt(0)) + word.substring(1));
        }
        return stringBuilder.toString();
    }
}
