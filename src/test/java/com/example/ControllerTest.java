package com.example;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class ControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    void camelizeShouldConvertToCamelCase() throws Exception {
        mockMvc.perform(get("/camelize?original=change_me"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("changeMe")));
    }

    @Test
    void camelizeShouldConvertToPascalCase() throws Exception {
        mockMvc.perform(get("/camelize?original=change_me&initialCap=true"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("ChangeMe")));

    }

    @Test
    void redactShouldRemoveBadWords() throws Exception {
        mockMvc.perform(get("/redact?original=A little of this and a little of that&badWord=little&badWord=this"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("A ****** of **** and a ****** of that")));
    }

    @Test
    void encodeShouldReplaceLetters() throws Exception {
        mockMvc.perform(
                post("/encode")
                        .param("message", "a little of this and a little of that")
                        .param("key", "mcxrstunopqabyzdefghijklvw"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("m aohhas zt hnog myr m aohhas zt hnmh")));
    }

    @Test
    void sedShouldReplaceAllWords() throws Exception{
        mockMvc.perform(
                post("/s/little/lot")
                        .content("a little of this and a little of that"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("a lot of this and a lot of that")));
    }
}